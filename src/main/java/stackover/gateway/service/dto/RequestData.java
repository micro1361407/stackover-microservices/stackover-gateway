package stackover.gateway.service.dto;

import io.jsonwebtoken.Claims;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "RequestData", description = "Проверка токена и извлечение из него данных")
public record RequestData(
        @Schema(description = "JWToken")
        String token,
        @Schema(description = "Claims")
        Claims claims) {
}
