package stackover.gateway.service.config;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import stackover.gateway.service.enums.ServiceLocation;
import stackover.gateway.service.filter.AuthenticationFilter;

@Configuration
@EnableHystrix
@RequiredArgsConstructor
public class RouterConfig {

    private final AuthenticationFilter authenticationFilter;

    @Bean
    public RouteLocator commonRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("auth-service-mapping",
                        route -> route
                                .path("/api/auth/**")
                                .filters(f -> f.filter(authenticationFilter))
                                .uri(ServiceLocation.STACKOVER_AUTH_SERVICE.getUri())
                )
                .route("profile-service-mapping",
                        route -> route
                                .path("/profile/**")
                                .filters(f -> f.filter(authenticationFilter))
                                .uri(ServiceLocation.STACKOVER_PROFILE_SERVICE.getUri()))
                .route("resource-service-mapping",
                        route -> route
                                .path("/resource/**")
                                .filters(f -> f.filter(authenticationFilter))
                                .uri(ServiceLocation.STACKOVER_RESOURCE_SERVICE.getUri()))
                .build();
    }

}
