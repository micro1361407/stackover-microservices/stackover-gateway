package stackover.gateway.service.config;

import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Configuration
public class OpenApiDefinitionLocatorConfig {

    @Autowired
    public OpenApiDefinitionLocatorConfig(RouteDefinitionLocator locator) {
        this.locator = locator;
    }

    final RouteDefinitionLocator locator;

    @Bean
    public List<GroupedOpenApi> apis() {
        List<GroupedOpenApi> groups = new ArrayList<>();
        List<RouteDefinition> definitions = locator
                .getRouteDefinitions().collectList().block();
        assert definitions != null;
        definitions.stream().filter(routeDefinition -> routeDefinition
                        .getId()
                        .matches(".*-service"))
                .forEach(routeDefinition -> {
                    String name = routeDefinition.getId()
                            .replaceAll("-service", "");
                    log.info(name);
                    groups.add(GroupedOpenApi.builder()
                            .pathsToMatch("/" + name + "/**").group(name).build());
                });
        return groups;
    }
}
