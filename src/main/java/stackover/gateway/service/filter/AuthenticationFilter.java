package stackover.gateway.service.filter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import stackover.gateway.service.dto.RequestData;
import stackover.gateway.service.exception.ValidateRequestException;
import stackover.gateway.service.util.JWTokensGatewayProvider;

import java.util.Collections;
import java.util.List;

@Slf4j
@RefreshScope
@Component
@RequiredArgsConstructor
public class AuthenticationFilter implements GatewayFilter {

    private final RouteValidator routeValidator;
    private final JWTokensGatewayProvider jwt;

    private static final String AUTHORIZATION_HEADER_MISSING_OR_INVALID = "Authorization header is missing or invalid";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        ServerHttpRequest request = exchange.getRequest();

        if (routeValidator.isSecured.test(request)) {
            if (this.isAuthMissing(request)) {
                return this.onError(exchange);
            }

            try {

                var authorizationToken = this.getAuthHeader(request);
                validateAndGetRequestData(exchange, authorizationToken);

            } catch (ValidateRequestException e) {
                log.error(AUTHORIZATION_HEADER_MISSING_OR_INVALID, e);
                return this.onError(exchange);
            }
        }

        return chain.filter(exchange);
    }

    private Mono<Void> onError(ServerWebExchange exchange) {

        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        return response.setComplete();
    }

    private boolean isAuthMissing(ServerHttpRequest request) {
        return Boolean.FALSE.equals(request.getHeaders().containsKey(HttpHeaders.AUTHORIZATION));
    }

    private String getAuthHeader(ServerHttpRequest request) throws ValidateRequestException {

        HttpHeaders headers = request.getHeaders();
        List<String> authorizationHeaders = headers.getOrDefault(HttpHeaders.AUTHORIZATION, Collections.emptyList());

        if (authorizationHeaders.isEmpty() || !authorizationHeaders.get(0).startsWith("Bearer: ")) {
            throw new ValidateRequestException(AUTHORIZATION_HEADER_MISSING_OR_INVALID);
        }

        return authorizationHeaders.get(0).substring(7);
    }

    public RequestData validateAndGetRequestData(ServerWebExchange exchange, String authorizationToken) throws ValidateRequestException {

        if (StringUtils.isBlank(authorizationToken) || Boolean.FALSE.equals(jwt.validateToken(authorizationToken))) {

            throw new ValidateRequestException(AUTHORIZATION_HEADER_MISSING_OR_INVALID);
        }

        var claims = jwt.getClaims(authorizationToken);
        exchange.getRequest().mutate()
                .header("login", claims.getSubject())
                .build();

        return new RequestData(authorizationToken, claims);
    }
}