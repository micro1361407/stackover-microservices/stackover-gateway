package stackover.gateway.service.filter;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;

@Service
public class RouteValidator {

    private RouteValidator() {}

    public static final List<String> endpoints = List.of(
            "/api/auth/login",
            "/api/auth/signup",
            "/api/auth/validate"
    );

    public Predicate<ServerHttpRequest> isSecured =
            request -> endpoints.stream().noneMatch(uri -> request.getURI().getPath().contains(uri));
}
